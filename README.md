# Sagano Builder

This image is used to build [Project Sagano](https://gitlab.com/CentOS/cloud/sagano) bootable base container images,
and will also help build disk images.

It will merge code slowly over time from:

- https://gitlab.com/CentOS/automotive/sample-images
- https://github.com/coreos/coreos-assembler
